package com.sp.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AllMethods {

	WebDriver driver ;
	
	public void appLaunch() 
	{
		System.setProperty("webdriver.chrome.driver", "./SeleniumDrivers/chromedriver.exe") ;
		driver =  new ChromeDriver() ;
		//driver.wait(5000);
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/test/newtours");
		//driver.close();
		
	}
	
	
	public void login()
	{
		
		WebElement uname = driver.findElement(By.name("userName"));
		uname.sendKeys("debasree");
		
		//WebElement pwd = driver.findElement(By.name("password")) ;
		WebElement pwd = driver.findElement(By.xpath("//input[@type='password']"));
		pwd.sendKeys("debasreepal");
		
		//WebElement submit = driver.findElement(By.name("submit"));
		WebElement submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		submit.click();
				
	}
	
	
	
	public void loginwithparam(String uname, String pwd) throws InterruptedException
	{
		WebElement username = driver.findElement(By.xpath("//input[@name='userName']"));
		username.sendKeys(uname);
		
		WebElement passwd = driver.findElement(By.xpath("//input[@type='password']"));
		passwd.sendKeys(pwd);
		
		WebElement submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		submit.click();
		
		Thread.sleep(6000);
				
	}
	


	public void closeApp()
	
	{
		driver.close();
	}
	
	
	public void verifyLogin()
	{
		String  expTitle = "Login: Mercury Tours" ;
		String  actTitle = driver.getTitle();
		
		if (actTitle.equals(expTitle))
		{
			System.out.println("Login Successful");
		}
		else
		{
			System.out.println("Login failed");
		}
		
	}
	
	
	public void departFlight() throws InterruptedException
	{
		
		WebElement flight = driver.findElement(By.linkText("Flights"));
		flight.click();
		
		Thread.sleep(2000);
		
		WebElement flightType = driver.findElement(By.xpath("//input[@value='oneway']"));
		flightType.click();
		
		
		Thread.sleep(2000);
		
		WebElement passnum = driver.findElement(By.name("passCount"));
		Select s1 = new Select(passnum);
		s1.selectByValue("3");
		
		Thread.sleep(2000);
		
		WebElement departfrom = driver.findElement(By.name("fromPort"));
		Select s2 = new Select(departfrom);
		s2.selectByVisibleText("Portland");
		//s2.selectByValue("New York");
		//s2.selectByIndex(8);
		
		//Thread.sleep(2000);
		
		WebElement departmonth = driver.findElement(By.xpath("//select[@name='fromMonth']"));
		Select s3 = new Select(departmonth) ;
		s3.selectByValue("10");
		
		Thread.sleep(2000);
		
		WebElement departdate = driver.findElement(By.xpath("//select[@name='fromDay']"));
		Select s4 = new Select(departdate) ;
		s4.selectByVisibleText("1");
		
		Thread.sleep(4000);		
				
	}
	
	public void arriveFlight() throws InterruptedException
	{
		WebElement arriveto = driver.findElement(By.name("toPort"));
		Select s2 = new Select(arriveto);
		s2.selectByVisibleText("Zurich");
		//s2.selectByValue("New York");
		//s2.selectByIndex(8);
		
		Thread.sleep(4000);
		
		WebElement arrivemonth = driver.findElement(By.xpath("//select[@name='toMonth']"));
		Select s3 = new Select(arrivemonth) ;
		s3.selectByValue("12");
		
		Thread.sleep(2000);
		
		WebElement arrivedate = driver.findElement(By.xpath("//select[@name='toDay']"));
		Select s4 = new Select(arrivedate) ;
		s4.selectByVisibleText("25");
		
		Thread.sleep(4000);
		
		
	}
	
	public void flightPref() throws InterruptedException
	
	{
		WebElement flightclass = driver.findElement(By.xpath("//input[@value='Business']"));
		flightclass.click();
		
		WebElement airline = driver.findElement(By.xpath("//select[@name='airline']"));
		Select s5 = new Select(airline);
		s5.selectByVisibleText("Blue Skies Airlines");
		
		Thread.sleep(4000);
		
		WebElement cont = driver.findElement(By.xpath("//input[@name='findFlights']"));
		cont.click();
		
	}
	
	
	/**
	 * 
	 * @param uname
	 * @param password
	 * @throws InterruptedException
	 */
	public void mouseActionLogins(String uname,String password) throws InterruptedException
	
	{
		WebElement username = driver.findElement(By.xpath("//input[@name='userName']"));
		username.sendKeys(uname) ;
		
		WebElement passwd = driver.findElement(By.xpath("//input[@type='password']"));
		passwd.sendKeys(password);
		
		WebElement submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		
		Actions builder = new Actions(driver) ;
		//builder.moveToElement(submit).build().perform();
		//builder.click().build().perform();
		//builder.moveToElement(submit).click().build().perform();
		
		builder.moveToElement(submit).clickAndHold().release().build().perform();
		
		
		Thread.sleep(6000);	
		
	}
	
	
	/**
	 * 
	 * @throws AWTException
	 * @throws InterruptedException
	 */
	
	public void keyboardLogin() throws AWTException, InterruptedException
	{	
		Robot r1 = new Robot();

		
		WebElement username = driver.findElement(By.name("userName"));
		username.click();
		
		
		r1.keyPress(KeyEvent.VK_D);
		r1.keyRelease(KeyEvent.VK_D);
		r1.keyPress(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_E);
		r1.keyPress(KeyEvent.VK_B);
		r1.keyRelease(KeyEvent.VK_B);
		r1.keyPress(KeyEvent.VK_A);
		r1.keyRelease(KeyEvent.VK_A);
		r1.keyPress(KeyEvent.VK_CAPS_LOCK);
		r1.keyPress(KeyEvent.VK_S);
		r1.keyRelease(KeyEvent.VK_S);
		r1.keyPress(KeyEvent.VK_R);
		r1.keyRelease(KeyEvent.VK_R);
		r1.keyPress(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_E);
		r1.keyPress(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_CAPS_LOCK);
		
		Thread.sleep(2000);
		
		r1.keyPress(KeyEvent.VK_TAB);
		r1.keyRelease(KeyEvent.VK_TAB);
		
		r1.keyPress(KeyEvent.VK_D);
		r1.keyRelease(KeyEvent.VK_D);
		r1.keyPress(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_E);
		r1.keyPress(KeyEvent.VK_B);
		r1.keyRelease(KeyEvent.VK_B);
		r1.keyPress(KeyEvent.VK_A);
		r1.keyRelease(KeyEvent.VK_A);
		r1.keyPress(KeyEvent.VK_S);
		r1.keyRelease(KeyEvent.VK_S);
		r1.keyPress(KeyEvent.VK_R);
		r1.keyRelease(KeyEvent.VK_R);
		r1.keyPress(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_E);
		r1.keyPress(KeyEvent.VK_E);
		r1.keyRelease(KeyEvent.VK_E);
		
		
		Thread.sleep(2000);
		
		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_ENTER);
		
		
	}
	
	public void loginImplicitwait() 
	{
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS) ;
		System.out.println("Waiting for username to be entered");
		WebElement username = driver.findElement(By.xpath("//input[@name='userName']"));
		username.sendKeys("debasree");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS) ;
		System.out.println("Waiting for password to be entered");
		WebElement passwd = driver.findElement(By.xpath("//input[@type='password']"));
		passwd.sendKeys("debasree");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS) ;
		System.out.println("Waiting for submit button to be clicked");
		WebElement submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		submit.click();
		
		
	}
	
	
	/**
	 * 
	 */
	public void loginExplicitwait() 
	
	{
		new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='userName']"))) ;
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']")) ;
		uName.sendKeys("debasree");
		
		new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']"))) ;
		WebElement passwd = driver.findElement(By.xpath("//input[@type='password']"));
		passwd.sendKeys("debasree");
		
		new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Submit']"))) ;		
		
		WebElement submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		submit.click();
		
		
		
	}
	
	/**
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void PropertyFileLogin() throws IOException, InterruptedException
	{
		File f1 = new File("./ExternalData/Sample1.properties");
		FileInputStream fis = new FileInputStream(f1);
		Properties prop = new Properties();
		prop.load(fis);
		Thread.sleep(2000);
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']")) ;
		uName.sendKeys(prop.getProperty("Username"));
		WebElement passwd = driver.findElement(By.xpath("//input[@type='password']"));
		passwd.sendKeys(prop.getProperty("Password"));
		Thread.sleep(2000);
		WebElement submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		submit.click();
		
	}
	
	
}